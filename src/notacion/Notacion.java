/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notacion;

/**
 *
 * @author user
 */
public class Notacion {

    char c[] = new char[0];
    Pila<Character> signos = new Pila<>();
    ListaS<String> l = new ListaS<>();

    public Notacion(String texto) {
        c = texto.toCharArray();
    }

    public void llenarPostfijo() {
        String num = "";
        for (int i = 0; i < c.length; i++) {

            if (c[i] > 47 && c[i] < 58) {
                num += c[i];
                if (i == c.length - 1) {
                    l.insertarAlFinal(num);
                }
                //l.insertarAlFinal(c[i] + "");
            } else {
                if (num != "") {
                    l.insertarAlFinal(num);
                    num = "";
                }
                if (c[i] == '(') {
                    signos.apilar('*');
                    signos.apilar(c[i]);

                } else if (signos.esVacia()) {

                    signos.apilar(c[i]);

                } else {
                    char tmp = signos.getTope();
                    if (tmp == '(' && c[i] == ')') {

                        signos.desapilar();

                    } else if (esMayorOp(c[i], tmp)) {
                        l.insertarAlFinal(signos.desapilar() + "");
                        signos.apilar(c[i]);

                    } else {
                        signos.apilar(c[i]);

                    }

                }

            }

        }

        while (!signos.esVacia()) {
            String tt = signos.desapilar() + "";
            if (!tt.equals("(") && !tt.equals(")")) {
                l.insertarAlFinal(tt);
            }

        }
    }

    public boolean esMayorOp(char m, char n) {

        int x = -1, y = -1;
        char a[] = {'-', '+', '*', '/'};
        for (int i = 0; i < a.length; i++) {
            if (m == a[i]) {
                x = i;
            }
            if (n == a[i]) {
                y = i;
            }
        }
        return y > x;
    }

    public int hallarResultado() {

        Pila<Integer> resul = new Pila<>();
        int n1, n2;

        for (int i = 0; i < l.getTamanio(); i++) {

            if (l.get(i).equals("+")) {
                n1 = resul.desapilar();
                n2 = resul.desapilar() + n1;
                resul.apilar(n2);
            } else if (l.get(i).equals("-")) {
                n1 = resul.desapilar();
                n2 = resul.desapilar() - n1;
                resul.apilar(n2);
            } else if (l.get(i).equals("*")) {
                n1 = resul.desapilar();
                n2 = resul.desapilar() * n1;
                resul.apilar(n2);
            } else if (l.get(i).equals("/")) {
                n1 = resul.desapilar();
                n2 = resul.desapilar() / n1;
                resul.apilar(n2);
            } else {

                resul.apilar(Integer.parseInt(l.get(i)));
            }
        }
        return resul.desapilar();
    }

    public String postfijo() {

        String rta = "";

        for (int i = 0; i < l.getTamanio(); i++) {

            rta += l.get(i) + " ";
        }
        return rta;
    }

    public String prefijo() {

        String rta = "", num = "";
        ListaS<String> tmp = new ListaS<>();
        Pila<Character> pi = new Pila<>();
        for (int i = 0; i < c.length; i++) {

            if (c[i] > 47 && c[i] < 58) {
                num += c[i];
                if (i == c.length - 1) {
                    tmp.insertarAlFinal(num+" ");
                }
                //tmp.insertarAlFinal(c[i] + "");
            } else {
                    if(num!=""){
                tmp.insertarAlFinal(num+" ");
                num= "";}
                if (c[i] == '(') {
                    signos.apilar('*');
                    signos.apilar(c[i]);
                } else if (signos.esVacia()) {

                    signos.apilar(c[i]);
                } else {
                    char tm = signos.getTope();
                    if (tm == '(' && c[i] == ')') {

                        signos.desapilar();

                    } else if (esMayorOp(c[i], tm)) {
                        tmp.insertarAlInicio(signos.desapilar() + " ");
                        signos.apilar(c[i]);
                    } else {
                        signos.apilar(c[i]);
                    }

                }

            }

        }
        while (!signos.esVacia()) {
            String tt = signos.desapilar() + "";
            if (!tt.equals("(") && !tt.equals(")")) {
                tmp.insertarAlInicio(tt);
            }

        }
        for (int i = 0; i < tmp.getTamanio(); i++) {
            rta += tmp.get(i) + " ";
        }
        return rta;
    }

    public char[] getC() {
        return c;
    }

    public void setC(char[] c) {
        this.c = c;
    }

    public Pila<Character> getSignos() {
        return signos;
    }

    public void setSignos(Pila<Character> signos) {
        this.signos = signos;
    }

    /*  public static void main(String[] args) {
        Notacion n = new Notacion("(2+3)/5");

        n.llenarPosfijo();

        System.out.println("cadena posfijo " + n.postfijo());

        System.out.println("Resultado: ");
        System.out.println(n.hallarResultado());
        System.out.println("cadena prefijo " + n.prefijo());

    }*/
}
